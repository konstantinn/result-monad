# Result
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'result-monad'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install result-monad

## Usage

Use of the Result Monad is a paradigm that is baked into Rust and can lead to very good error handling.  This is an
initial design, and will need to be refined.  The `Capture()` constructor makes it quite easy to integrate Result into
a preexisting code base.

### Examples

#### `Capture` success into an Ok result, and exceptions into an Error result via

```ruby

Capture { 100 / 10 } #=> Result<Ok(10)>

Capture { 100 / 0 } #=> Result<Error(divided by 0)>

```

#### Construct Ok or Error yourself

```ruby
  def can_go_wrong
    res = do_operation
    if res.meets_expectations
      Ok(res)
    else
      Error(res)
    end
  end

```

#### Basic Dichotomy

```ruby

def divide(x, y)
  Capture { x / y }
end

x = divide 50, 10 #=> Result<Ok(5)>
x.ok? #=> true
x.error? #=> false

y = divide 50, 0 #=> Result<Error(divided by 0)>
y.error? #=> true
y.ok? #=> false
```

#### Chaining computations with map

```ruby
x = Ok(100)

x.map {|i| i / 10}
 .map {|i| i / 5}
 .map {|i| i / 1}
#=> Result<Ok(2)>
```

#### Carrying errors through computations

```ruby
x = Ok(100)

x.map {|i| i / 10}
 .map {|i| i / 0}
 .map {|i| i / 10}
 
#=>Result<Error(divided by 0)>
```

#### Performing actions in sequence

```ruby
x = Ok(5)
y = Error(99)
z = Ok(25)

# In this case, imagine x and z as successful actions, whereas y is an unsuccessful action.

x & y & z #=> Result<Error(99)>

```

#### Real World Scenario

Lets say we are using an external library to read data from a file over FTP,
write it to a local file, and then delete the file on the FTP server.  Some things to note:

* We use overloaded operator & to chain on Ok
* Every call to any outside library, even standard library functions are either in a `map` block which
  gets Captured or they are themselves Captured
* No independent action (as in someone calling `copy_file` directly) will execute without its requirements being met.
* A shortcut to creating an Error value based on some condition, as well as capturing errors generated from testing the
  condition can be seen in `ensure_file`.  Any exceptions raised by File.exist?, as well as our condition not being met
  are captured as Error

```ruby
require 'net/ftp'

class SensitiveFileFTP
  def initialize(server_uri)
    @server_uri = server_uri
  end
  
  def transfer_and_delete(file_name)
    login & # "&" chains on Ok
    copy_file(file_name) & 
    ensure_file(file_name) & 
    delete_remote_file(file_name)
  end
  
  def ftp
    @ftp ||= Capture { Net::FTP.new(@server_uri) }
  end
  
  def login
    ftp.map(&:login)
  end
  
  def copy_file(source)
    ftp.map { |f| f.getbinaryfile(source) }
  end
  
  def ensure_file(name)
    Capture { raise "Can not verify local copy of file" unless File.exist?(name) }
  end
  
  def delete_remote_file(name)
    ftp.map {|f| f.delete(name) }
  end
  
  def delete_local_file(name)
    Capture { File.delete(name) }
  end
end

ftp = SensitiveFileFTP.new('speedtest.tele2.net')
ftp.transfer_and_delete("5MB.zip")
```

`transfer_and_delete` can return numerous errors but there is no way for this code to raise an exception.  Exceptions
raised in `map` or `map!` are caught and expressed as `Error`.

#### Conditionally executing code

Using the example above, say we wanted to find and delete the file if we may have accidentally created it.
This wouldn't be a sensible way of solving this problem, but this illustrates a use case for result base conditionals.

```ruby
ftp = SensitiveFileFTP.new('speedtest.tele2.net')
ftp.transfer_and_delete("5MB.zip").or_else do
  ftp.delete_local_file("5MB.zip")
end

# or lets say we wanted to log a success if it worked

ftp.transfer_and_delete("5MB.zip").and_then do
  log "Did the thing!"
end
```


## What's next

* More documentation and refined use cases for existing methods, like `map_err`, `map!`, and `|`

* refine methods of conditionally executing blocks.  Should the return values be wrapped in results, or should we trust
  the users to do it?

* Right now, the only computational state kept is the last good result or the first error that occurred.
This may not be ideal or appropriate.  We also don't join errors up the chain, so it is possible to end up with
something like this: `Error(Error(Error(Error(Error(...)))))`.  We either need to find a way to join, and perhaps
store the errors as some aggregate object, or we need to provide utilities for probing for information on the different
nested errors.  I think it may be appropriate to kep a collection of errors, and ensure that we don't nest.


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/result. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).