require 'spec_helper'

describe Result do
  let (:good) { "A good result!" }
  let (:bad)  { Exception.new("Error") }
  let (:err)  { Error(bad) }
  let (:okay) { Ok(good) }

  it 'has a version number' do
    expect(Result::VERSION).not_to be nil
  end

  it 'provides the Error constructor' do
    expect(Error(Exception.new("Error"))).to be_an_instance_of(Result)
  end

  it 'provides the Ok constructor' do
    expect(Ok("A good result!")).to be_an_instance_of(Result)
  end

  describe 'Capture' do
    it 'returns a Result::Ok if no exception' do
      expect(Capture {"A good result!"}).to be_ok
    end

    it 'returns a Result::Error if an exception occurs' do
      expect(Capture {raise "an error"}).to be_error
    end
  end

  describe 'unwrap' do
    context 'error' do
      it 'raises an exception' do
        expect {err.unwrap}.to raise_exception(ResultError)
      end
    end

    context 'ok' do
      it 'returns the wrapped value' do
        expect(okay.unwrap).to eq(good)
      end
    end
  end

  describe 'map' do
    context 'error' do
      it 'leaves the object alone' do
        expect(err.map {|x| x.not_a_method_on_error}).to eq(err)
      end

      it 'does not call the provided block' do
        proc = Proc.new {|x| x.still_not_a_method}
        expect(proc).not_to receive(:call)
        err.map &proc
      end
    end

    context 'ok' do
      it 'calls the provided block' do
        proc = Proc.new {|x| x.split}
        expect(proc).to receive(:call)
        okay.map &proc
      end

      it 'returns the value of the block called with x' do
        proc = Proc.new {|x| x + "Something Else"}
        expect(okay.map(&proc).unwrap).to eq(good + "Something Else")
      end
    end
  end

  describe '&' do
    context "ok" do
      it 'returns the right side if left side is okay' do
        expect(okay & :right).to eq(:right)
      end
    end

    context "error" do
      it 'returns the left side if the left side is error' do
        expect(err & :right).to eq(err)
      end
    end
  end

  describe "unwrap_or" do
    context "ok" do
      it 'returns the value' do
        expect(okay.unwrap_or(:else)).to eq(good)
      end
    end

    context "error" do
      it 'returns the else' do
        expect(err.unwrap_or(:else)).to eq(:else)
      end
    end
  end

  describe "or_else" do
    context "ok" do
      it 'does not execute the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect(proc).not_to receive(:call)
        okay.or_else(&proc)
      end
    end

    context "error" do
      it 'executes the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect(proc).to receive(:call)
        err.or_else(&proc)
      end
    end
  end

  describe "and_then" do
    context "ok" do
      it 'executes the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect(proc).to receive(:call)
        okay.and_then(&proc)
      end
    end

    context "error" do
      it 'does not execute the provided block' do
        proc = Proc.new {puts "Whatsup"}
        expect(proc).not_to receive(:call)
        err.and_then(&proc)
      end
    end
  end

  describe "map_when" do
    context 'kind matches' do
      it 'executes the block' do
        proc = Proc.new{|x| x}
        expect(proc).to receive(:call)
        okay.map_when(String, &proc)
      end

      it 'sends self as an argument' do
        expect(good).to receive(:split)
        okay.map_when(String) { |x|
          x.split
        }
      end
    end

    context 'kind does not match' do
      it 'does nothing and returns self' do
        proc = Proc.new{|x| x}
        expect(proc).not_to receive(:call)
        okay.map_when(Fixnum, &proc)
      end
    end
  end

  describe "do_when" do
    context 'kind matches' do
      it 'executes the block' do
        proc = Proc.new{"what"}
        expect(proc).to receive(:call)
        okay.map_when(String, &proc)
      end
    end

    context 'kind does not match' do
      it 'does nothing and returns self' do
        proc = Proc.new{"what"}
        expect(proc).not_to receive(:call)
        okay.map_when(Fixnum, &proc)
      end
    end
  end

  describe "raise_or_return!" do
    context "ok" do
      it "returns the wrapped value" do
        expect(okay.raise_or_return!).to eq(good)
      end
    end

    context "err" do
      it "raises the wrapped value" do
        expect {err.raise_or_return! }.to raise_exception(bad)
      end
    end
  end
end
