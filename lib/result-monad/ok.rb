class Ok
  def initialize(value)
    @value = value
  end

  def map!(&block)
    @value = Capture { block.call(@value) }
    self
  end

  def map(&block)
    Capture { block.call(@value) }
  end

  def to_s
    "Ok(#{@value})"
  end
end