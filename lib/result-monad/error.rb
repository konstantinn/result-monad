class Error
  def initialize(value)
    @value = value
  end

  def map!(&block)
    @value = Capture {block.call(@value)}
    self
  end

  def map(&block)
    capture_as_error { block.call(@value) }
  end

  def to_s
    "Error(#{@value})"
  end

  private
  def capture_as_error
    begin
      Error(yield)
    rescue StandardError => e
      Error(e)
    end
  end
end