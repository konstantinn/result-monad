require "result-monad/version"
require 'byebug'

def Error(error)
  Result.error(error)
end

def Ok(value)
  Result.ok(value)
end

def Capture
  begin
    Ok(yield).join!
  rescue StandardError => e
    Error(e).join!
  end
end

class Result
  require 'result-monad/error'
  require 'result-monad/ok'

  def initialize(m)
    @m = m
  end

  def ok?
    @m.is_a? Ok
  end

  def error?
    @m.is_a? Error
  end

  def map!(&block)
    if ok?
      @m.map!(&block)
      self.lift!(val)
    else
      self
    end
  end

  def map_err!(&block)
    if error?
      @m.map!(&block)
    else
      self
    end
  end

  def map(&block)
    if ok?
      @m.map(&block)
    else
      self
    end
  end

  def map_err(&block)
    if error?
      @m.map(&block)
    else
      self
    end
  end

  def join!
    if ok? && val.is_a?(Result)
      lift!(val)
    end
    self
  end

  def lift!(other)
    @m = (self & other.join!).instance_variable_get(:"@m")
    self
  end

  def unwrap
    raise ResultError.new(self), "Attempted to unwrap an error" if @m.is_a? Error
    val
  end

  def and(other)
    error? ? self : other
  end
  alias_method :&, :and

  def or(other)
    error? ? other : self
  end
  alias_method :|, :or

  def and_then(&block)
    Capture {block.call} if ok?
  end

  def or_else(&block)
    Capture {block.call} if error?
  end

  def unwrap_or(cons)
    ok? ? unwrap : cons
  end

  def map_when(kind, &block)
    @m.map(&block) if val.is_a?(kind)
    self
  end

  def do_when(kind, &block)
    Capture { block.call } if val.is_a?(kind)
    self
  end

  def raise_or_return!
    if ok?
      return val
    else
      raise val
    end
  end

  # Constructors
  def self.error(error)
    new(Error.new(error))
  end

  def self.ok(result)
    new(Ok.new(result))
  end

  def to_s
    @m.to_s
  end

  def inspect
    "Result<#{self}>"
  end

  private

  def val
    @m.instance_variable_get(:"@value")
  end
end

class ResultError < StandardError
  attr_reader :result

  def initialize(result)
    @result = result
  end
end
